package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.Empleado;

public interface EmpleadoService {
	
	List<Empleado> obtenerEmpleados();
	
	Empleado obtenerEmpleado(Integer id);
	
	void insertarEmpleado(Empleado comentario);

	Empleado editarEmpleado(Integer id, Empleado e);
	
	void eliminarEmpleado(Integer id);
}
